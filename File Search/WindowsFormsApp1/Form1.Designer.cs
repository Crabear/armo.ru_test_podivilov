﻿namespace FIleSearch
{
	partial class Form1
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Узел0");
			this.buttonSearch = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.TextString = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			this.button2 = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.CurrentFile = new System.Windows.Forms.TextBox();
			this.Update = new System.Windows.Forms.Timer(this.components);
			this.buttonPause = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.Tree = new System.Windows.Forms.TreeView();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label4 = new System.Windows.Forms.Label();
			this.CountText = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.TimeText = new System.Windows.Forms.TextBox();
			this.FolderText = new System.Windows.Forms.TextBox();
			this.NameString = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonSearch
			// 
			this.buttonSearch.Location = new System.Drawing.Point(182, 97);
			this.buttonSearch.Name = "buttonSearch";
			this.buttonSearch.Size = new System.Drawing.Size(100, 23);
			this.buttonSearch.TabIndex = 0;
			this.buttonSearch.Text = "Поиск";
			this.buttonSearch.UseVisualStyleBackColor = true;
			this.buttonSearch.Click += new System.EventHandler(this.SearchButton_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 48);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(119, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Шаблон имени файла ";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 74);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(164, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "Текст, содержащийся в файле";
			// 
			// TextString
			// 
			this.TextString.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FIleSearch.Properties.Settings.Default, "TextPart", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.TextString.Location = new System.Drawing.Point(182, 71);
			this.TextString.Name = "TextString";
			this.TextString.Size = new System.Drawing.Size(100, 20);
			this.TextString.TabIndex = 3;
			this.TextString.Text = global::FIleSearch.Properties.Settings.Default.TextPart;
			this.TextString.TextChanged += new System.EventHandler(this.TextString_TextChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 22);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(108, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Директория поиска";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(288, 17);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(78, 23);
			this.button2.TabIndex = 7;
			this.button2.Text = "Указать";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(384, 22);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(120, 13);
			this.label5.TabIndex = 11;
			this.label5.Text = "Текущий файл поиска";
			// 
			// CurrentFile
			// 
			this.CurrentFile.Location = new System.Drawing.Point(384, 45);
			this.CurrentFile.Multiline = true;
			this.CurrentFile.Name = "CurrentFile";
			this.CurrentFile.Size = new System.Drawing.Size(241, 78);
			this.CurrentFile.TabIndex = 10;
			// 
			// Update
			// 
			this.Update.Interval = 10;
			this.Update.Tick += new System.EventHandler(this.Update_Tick);
			// 
			// buttonPause
			// 
			this.buttonPause.Location = new System.Drawing.Point(182, 126);
			this.buttonPause.Name = "buttonPause";
			this.buttonPause.Size = new System.Drawing.Size(100, 23);
			this.buttonPause.TabIndex = 12;
			this.buttonPause.Text = "Пауза";
			this.buttonPause.UseVisualStyleBackColor = true;
			this.buttonPause.Click += new System.EventHandler(this.Pause_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(182, 155);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(100, 23);
			this.button1.TabIndex = 13;
			this.button1.Text = "Продолжить";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Resume_Click);
			// 
			// Tree
			// 
			this.Tree.Location = new System.Drawing.Point(10, 19);
			this.Tree.Name = "Tree";
			treeNode2.Name = "1";
			treeNode2.Text = "Узел0";
			this.Tree.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode2});
			this.Tree.Size = new System.Drawing.Size(731, 368);
			this.Tree.TabIndex = 14;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.Tree);
			this.groupBox1.Location = new System.Drawing.Point(15, 184);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(747, 393);
			this.groupBox1.TabIndex = 15;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Результаты";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(631, 22);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(105, 13);
			this.label4.TabIndex = 17;
			this.label4.Text = "Файлов проверено";
			// 
			// CountText
			// 
			this.CountText.Location = new System.Drawing.Point(631, 45);
			this.CountText.Multiline = true;
			this.CountText.Name = "CountText";
			this.CountText.Size = new System.Drawing.Size(120, 26);
			this.CountText.TabIndex = 16;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(631, 78);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(79, 13);
			this.label6.TabIndex = 19;
			this.label6.Text = "Время поиска";
			// 
			// TimeText
			// 
			this.TimeText.Location = new System.Drawing.Point(631, 97);
			this.TimeText.Multiline = true;
			this.TimeText.Name = "TimeText";
			this.TimeText.Size = new System.Drawing.Size(120, 26);
			this.TimeText.TabIndex = 18;
			// 
			// FolderText
			// 
			this.FolderText.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FIleSearch.Properties.Settings.Default, "Folder", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.FolderText.Location = new System.Drawing.Point(182, 19);
			this.FolderText.Name = "FolderText";
			this.FolderText.Size = new System.Drawing.Size(100, 20);
			this.FolderText.TabIndex = 5;
			this.FolderText.Text = global::FIleSearch.Properties.Settings.Default.Folder;
			// 
			// NameString
			// 
			this.NameString.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::FIleSearch.Properties.Settings.Default, "NamePart", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.NameString.Location = new System.Drawing.Point(182, 45);
			this.NameString.Name = "NameString";
			this.NameString.Size = new System.Drawing.Size(100, 20);
			this.NameString.TabIndex = 1;
			this.NameString.Text = global::FIleSearch.Properties.Settings.Default.NamePart;
			this.NameString.TextChanged += new System.EventHandler(this.NameString_TextChanged);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(774, 589);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.TimeText);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.CountText);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.buttonPause);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.CurrentFile);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.FolderText);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.TextString);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.NameString);
			this.Controls.Add(this.buttonSearch);
			this.Controls.Add(this.groupBox1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button buttonSearch;
		private System.Windows.Forms.TextBox NameString;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox TextString;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox FolderText;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox CurrentFile;
		private System.Windows.Forms.Timer Update;
		private System.Windows.Forms.Button buttonPause;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TreeView Tree;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox CountText;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox TimeText;
	}
}

