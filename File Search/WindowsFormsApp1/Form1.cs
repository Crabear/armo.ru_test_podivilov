﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIleSearch
{
	public partial class Form1 : Form
	{

		private SearchTypes searchType = SearchTypes.ByName;
		private bool paused;
		private string fileName;
		private Stopwatch watch = new Stopwatch();
		private int currentResult;
		private TreeNode root = new TreeNode("Рeзультаты поиска");
		private int fileCount;
		private bool searching;

		public Form1()
		{
			InitializeComponent();
		}

	



		private void button2_Click(object sender, EventArgs e)
		{
			Pause_Click(sender,e);
			using (var fbd = new FolderBrowserDialog())
			{
				DialogResult result = fbd.ShowDialog();

				if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
				{
					FolderText.Text = fbd.SelectedPath;
					Finish();
				}
			}
		}

		public async void FindFiles()
		{
			watch.Restart();
		 await Task.Run(() => SearchCycle(FolderText.Text));
			
		}

		private void SearchCycle(string path)
		{
			var files = new string[0];

			try
			{
				 files = Directory.GetFiles(path);
			}
			catch (Exception) { }
			for (var index = 0; index < files.Length; index++)
			{
				fileName = files[index];
				fileCount ++;
				if (CheckMatches(fileName))
				{
					AddResult(fileName);
				}
				while (paused)
				{
					if(!searching)Search.Abort();
					Thread.Sleep(100);
				}

			}

			var paths = new string[0];

			try
			{
				 paths = Directory.GetDirectories(path);
			}
			catch (Exception) { }

			if (paths.Length == 0)
			{
				if (path == FolderText.Text) Finish();

				return;
			}

			for (var i = 0; i < paths.Length; i++)
			{
				SearchCycle(paths[i]);
			}
			if (path == FolderText.Text) Finish();

		}

		private void Finish()
		{
			paused = true;
			searching = false;
			watch.Stop();
			Search.Abort();

			Update.Enabled = false;
		}

		private bool CheckMatches(string s)
		{
			bool match;
			switch (searchType)
			{
			case SearchTypes.ByName:
				match = Path.GetFileName(s).Contains(NameString.Text);

				if (match)
					return true;
				else
					return false;

			case SearchTypes.ByText:

				try
				{
					match = File.ReadAllText(s).Contains(TextString.Text);

				}
				catch (Exception)
				{
					match = false;
				}
				if (match)
					return true;
				else
					return false;

			case SearchTypes.Couple:
				match = Path.GetFileName(s).Contains(NameString.Text);

				if (match)
				{
					match = File.ReadAllText(s).Contains(TextString.Text);

					if (match)
						return true;
					else
						return false;
				}
				else
					return false;

			default:
				return true;
			}
		}


		private TreeNode AddNode(TreeNode node, string key)
		{
			if (node.Nodes.ContainsKey(key))
			{
				return node.Nodes[key];
			}
			else
			{
				int indexImage = (int)Images.Folder;
				if (Path.GetExtension(key) != null || Path.GetExtension(key) == String.Empty)
					indexImage = (int)Images.File;
				return node.Nodes.Add(key, key, indexImage);
			}
		}


		private void AddResult(string filename)

		{
			Action action = () =>
			{

				TreeNode node = root;

				node = root;
				foreach (string pathBits in filename.Split('\\'))
				{
					node.Expand();
					node = AddNode(node, pathBits);
					node.Expand();
				}
			};
			if (InvokeRequired)
			{
				Invoke(action);
			}
			else
			{
				action();
			}
		}

		private Thread Search;

		private void SearchButton_Click(object sender, EventArgs e)
		{
			FIleSearch.Properties.Settings.Default.Save();

			Tree.Nodes.Clear();
			root.Nodes.Clear();
			Tree.Nodes.Add(root);

			fileCount = 0;
			currentResult = 0;
			searching = true;
			if (Search != null)
				Search.Abort();



			CheckSearchType();

			Search = new Thread(FindFiles);
			Search.Start();
			paused = false;
			Update.Enabled = true;
		}

		private void CheckSearchType()
		{
			searchType = SearchTypes.Couple;
			if (String.IsNullOrWhiteSpace(NameString.Text))
			{
				searchType = SearchTypes.ByText;
				if (String.IsNullOrWhiteSpace(TextString.Text))
				{
					searchType = SearchTypes.None;
				}
			}
			else if (String.IsNullOrWhiteSpace(TextString.Text))
			{
				searchType = SearchTypes.ByName;
			}

		}

		private void Update_Tick(object sender, EventArgs e)
		{
			UI_Update();
		}

		void UI_Update()
		{
			CurrentFile.Text = fileName;
			TimeText.Text = watch.ElapsedMilliseconds + @" мc";
			CountText.Text = fileCount.ToString();
		}

		private void Pause_Click(object sender, EventArgs e)
		{
			if (!searching) return;


			paused = true;
			Update.Enabled = false;

			UI_Update();
			watch.Stop();
		}

		private void Resume_Click(object sender, EventArgs e)
		{
			if (!searching) return;

			paused = false;
			Update.Enabled = true;
			watch.Start();
		}

		private void NameString_TextChanged(object sender, EventArgs e)
		{
			Finish();
		}

		private void TextString_TextChanged(object sender, EventArgs e)
		{
			 Finish();
		}
	}

	internal enum SearchTypes
	{
		ByName,
		ByText,
		Couple,
		None,
	}

	internal enum Images
	{
		Folder,
		File,
	}


	
}
